'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class USER_HAVE_LOANS extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  USER_HAVE_LOANS.init({
    status: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'USER_HAVE_LOANS',
  });
  return USER_HAVE_LOANS;
};
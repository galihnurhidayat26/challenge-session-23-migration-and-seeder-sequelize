'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class LOANS extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  LOANS.init({
    amount: DataTypes.STRING,
    due_date: DataTypes.STRING,
    loan_category_id: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'LOANS',
  });
  return LOANS;
};
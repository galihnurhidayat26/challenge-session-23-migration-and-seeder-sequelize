'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class LOAN_HAVE_LOAN_ACTIVITIES extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  LOAN_HAVE_LOAN_ACTIVITIES.init({
    date: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'LOAN_HAVE_LOAN_ACTIVITIES',
  });
  return LOAN_HAVE_LOAN_ACTIVITIES;
};